from django.contrib import admin

from .models import Question,Choice

class ChoiceInline(admin.TabularInline):		#Row level view
    model = Choice
    extra = 3

class QuestionAdmin(admin.ModelAdmin): 			#Table level view
    list_display = ('question_text','pub_date','was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question_text']
    inlines = [ChoiceInline]

class ChoiceAdmin(admin.ModelAdmin):			#table level view
	list_display = ('choice_text', 'votes')

admin.site.register(Question, QuestionAdmin)	#DB level view
admin.site.register(Choice, ChoiceAdmin)		#DB level view
from django.db import models
from django.utils import timezone
import datetime

class Album(models.Model):
	artist = models.CharField(max_length=250, blank=True, null=True)
	album_title = models.CharField(max_length=200, blank=True, null=True)
	genre = models.CharField(max_length=100, blank=True, null=True)
	album_logo = models.CharField(max_length=1000, blank=True, null=True)
	pub_date = models.DateTimeField('date published')
	
	def was_published_recently(self):
		return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
	was_published_recently.admin_order_field = 'pub_date'
	was_published_recently.boolean = True
	was_published_recently.short_description = 'Published recently?'
	
	def __str__(self):
		return self.album_title 
		# + ' - ' + self.artist

class Song(models.Model):
	album = models.ForeignKey(Album, on_delete=models.CASCADE)
	file_type = models.CharField(max_length=200, blank=True, null=True)
	song_title = models.CharField(max_length=200, blank=True, null=True)

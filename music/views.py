from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse

from .models import Album, Song

def index(request):
    latest_album_list = Album.objects.order_by('-pub_date')[:5]
    context = {'latest_album_list': latest_album_list}
    return HttpResponse('<h1> List of all Albums </h1>')
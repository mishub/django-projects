from django.conf.urls import include, url
from django.contrib import admin
from task1 import views

urlpatterns = [
    # Examples:
    # url(r'^$', 'sachin.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', views.func1),  
    url(r'^result/$', views.func1),
    url(r'^polls/', include('polls.urls')),
    url(r'^music/', include('music.urls')),
    url(r'^admin/', include(admin.site.urls)),
    ]